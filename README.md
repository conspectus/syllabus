# syllabus

Simple API to upload and retrieve syllabus information

## Notes

### Set Elasticsearch IP/host

Hardcoded in `no.oslomet.abi.metainfo.conspectus.spring.configuration.CitationsElasticsearchConfiguration.java`. Set to 
localhost:9200. Update accordingly. Consider making this a parameter configurable at startup. Consider locking ES down 
with BasicAuth.

### Getting started 

To start the application run:

```
mvn spring-boot:run
```

Assuming you have ES running the application should start on 8142 with context path '/conspectus'

```
curl -X GET  http://localhost:8142/conspectus/citations/
```
It will return an empty dataset unless the elasticsearch instance has data.

### Populating with data 

It is possible to [upload data with this script](https://codeberg.org/conspectus/syllabus-import).

```
curl -H 'Authorization: Basic YWRtaW5fcG9zdDpTRVRNRVRPU09NRVRISU5H' -X POST  http://localhost:8142/conspectus/citation
```

`Basic YWRtaW5fcG9zdDpTRVRNRVRPU09NRVRISU5H` corresponds with `admin_post:SETMETOSOMETHING`. These values should be set
to something else in production.

### Security

The application has no security for GET requests. There is a single POST definition that requires a Basic Auth header 
(no realm is defined), where the applicable username and password are set in the application.yml file. See 
`no.oslomet.abi.metainfo.conspectus.security.SyllabusWebSecurityConfig` if you wish to configure anything security 
related. Further, the application limits the POST request to being authenticated and coming from localhost.

## Get in touch

You can find the author on IRC, [OFTC IRC channel #conspectus](https://webchat.oftc.net/?randomnick=1&channels=%23conspectus&uio=d4). It is also possible to locate contact information by doing an internet search for 'Thomas Sødring OsloMet'
