package no.oslomet.abi.metainfo.conspectus.controller;

import no.oslomet.abi.metainfo.conspectus.domain.*;
import no.oslomet.abi.metainfo.conspectus.service.ICitationService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.PagedModel;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.OK;

@RestController
@CrossOrigin(origins = "*")
public class CitationController {

    private final ICitationService citationService;
    private final CitationModelAssembler citationModelAssembler;
    private final PagedResourcesAssembler<Citation> pagedResourcesAssembler;
    private final APIAssembler apiAssembler;

    public CitationController(ICitationService citationService,
                              CitationModelAssembler citationModelAssembler,
                              PagedResourcesAssembler<Citation> pagedResourcesAssembler,
                              APIAssembler apiAssembler) {
        this.citationService = citationService;
        this.citationModelAssembler = citationModelAssembler;
        this.pagedResourcesAssembler = pagedResourcesAssembler;
        this.apiAssembler = apiAssembler;
    }

    @GetMapping("/")
    public ResponseEntity<LinksAPIDetail> getRoot() {
        return ResponseEntity.status(OK).
                body(apiAssembler.toModel(new APIDetail()));
    }

    @GetMapping("/citations")
    public ResponseEntity<PagedModel<CitationModel>> getFiltered(
            @RequestParam Map<String, String> queryParameters, Pageable pageable) {
        if (queryParameters.containsKey("page") &&
                queryParameters.containsKey("size")) {
            pageable =
                    PageRequest.of(Integer.parseInt(queryParameters.get("page")),
                            Integer.parseInt(queryParameters.get("size")));
            queryParameters.remove("page");
            queryParameters.remove("size");
        }
        Page<Citation> citationPage = citationService.searchCitationsUsingMap(queryParameters, pageable);
        PagedModel<CitationModel> pagedModel = pagedResourcesAssembler
                .toModel(citationPage, citationModelAssembler);
        return new ResponseEntity<>(pagedModel, OK);
    }

    @GetMapping("/details")
    public ResponseEntity<Details> getDetails(
            @RequestParam Map<String, String> queryParameters) {
        Details details =
                citationService.getDetails(queryParameters);
        return ResponseEntity
                .status(OK)
                .body(details);
    }

    @GetMapping("/search/{queryString}")
    public ResponseEntity<PagedModel<CitationModel>> searchCitationAllFields(
            @PathVariable String queryString, Pageable pageable,
            @RequestParam Map<String, String> queryParameters) {
        pageable = removePageableFromQueryParameters(queryParameters, pageable);
        Page<Citation> citationPage =
                citationService.searchCitationsAnyField(
                        queryString, pageable, queryParameters);
        PagedModel<CitationModel> pagedModel = pagedResourcesAssembler
                .toModel(citationPage, citationModelAssembler);
        return new ResponseEntity<>(pagedModel, OK);
    }

    @PostMapping("/citation")
    public ResponseEntity<CitationModel> addCitation(@RequestBody Citation citation) {
        return new ResponseEntity<>(citationModelAssembler.toModel(
                citationService.addCitation(citation)), CREATED);
    }

    private Pageable removePageableFromQueryParameters(
            Map<String, String> queryParameters, Pageable pageable) {
        if (queryParameters.containsKey("page") &&
                queryParameters.containsKey("size")) {
            pageable =
                    PageRequest.of(Integer.parseInt(queryParameters.get("page")),
                            Integer.parseInt(queryParameters.get("size")));
            queryParameters.remove("page");
            queryParameters.remove("size");
        }
        return pageable;
    }
}
