package no.oslomet.abi.metainfo.conspectus.domain;

import no.oslomet.abi.metainfo.conspectus.controller.CitationController;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.stereotype.Component;

import static no.oslomet.abi.metainfo.conspectus.utils.Constants.PARAMETERS;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Component
public class APIAssembler extends
        RepresentationModelAssemblerSupport<APIDetail, LinksAPIDetail> {

   public APIAssembler() {
        super(CitationController.class, LinksAPIDetail.class);
    }

    @Override
    public LinksAPIDetail toModel(APIDetail aPIDetail) {
        LinksAPIDetail linksAPIDetail = instantiateModel(aPIDetail);
        linksAPIDetail.add(linkTo(methodOn(CitationController.class)
                .getFiltered(PARAMETERS, null))
                .withRel("filtered").expand(PARAMETERS));
        linksAPIDetail.add(linkTo(methodOn(CitationController.class)
                .searchCitationAllFields(null, null, null))
                .withRel("search"));
        return linksAPIDetail;
    }
}
