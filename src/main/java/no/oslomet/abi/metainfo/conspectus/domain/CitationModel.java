package no.oslomet.abi.metainfo.conspectus.domain;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.springframework.hateoas.RepresentationModel;
import org.springframework.hateoas.server.core.Relation;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;
import static no.oslomet.abi.metainfo.conspectus.utils.Constants.*;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Relation(collectionRelation = "citations", itemRelation = "citation")
@JsonInclude(NON_NULL)
public class CitationModel extends RepresentationModel<CitationModel> {
    @JsonProperty("citation_id")
    private String citationId;
    @JsonProperty(ORGANISATION_CODE)
    private String organisationCode;
    @JsonProperty(SUB_UNIT_CODE)
    private String subUnitCode;
    @JsonProperty(SUB_SUB_UNIT_CODE)
    private String subSubUnitCode;
    @JsonProperty(ORGANISATION_NAME)
    private String organisationName;
    @JsonProperty(SUB_UNIT_NAME)
    private String subUnitName;
    @JsonProperty(SUB_SUB_UNIT_NAME)
    private String subSubUnitName;
    @JsonProperty(COURSE_CODE)
    private String courseId;
    @JsonProperty(COURSE_YEAR)
    private String courseYear;
    @JsonProperty(COURSE_CODE_NAME)
    private String courseCodeName;
    @JsonProperty(COURSE_NAME)
    private String courseName;
    @JsonProperty("institute_code_b")
    private String instituteCodeB;
    @JsonProperty("semester_code_1")
    private String semesterCode1;
    @JsonProperty("semester_code_2")
    private String semesterCode2;
    @JsonProperty("semester_code_3")
    private String semesterCode3;
    @JsonProperty("term_code_1")
    private String termCode1;
    @JsonProperty("term_code_2")
    private String termCode2;
    @JsonProperty("term_code_3")
    private String termCode3;
    @JsonProperty("term_name_1")
    private String termName1;
    @JsonProperty("term_name_2")
    private String termName2;
    @JsonProperty("term_name_3")
    private String termName3;
    @JsonProperty("metadata_title")
    private String title;
    @JsonProperty("metadata_author")
    private String author;
    @JsonProperty("metadata_publisher")
    private String publisher;
    @JsonProperty("metadata_publication_date")
    private String publicationDate;
    @JsonProperty("metadata_edition")
    private String edition;
    @JsonProperty("metadata_isbn")
    private String isbn;
    @JsonProperty("metadata_issn")
    private String issn;
    @JsonProperty("metadata_mms_id")
    private String mmsId;
    @JsonProperty("metadata_additional_person_name")
    private String additionalPersonName;
    @JsonProperty("metadata_place_of_publication")
    private String placeOfPublication;
    @JsonProperty("metadata_call_number")
    private String callNumber;
    @JsonProperty("metadata_note")
    private String note;
    @JsonProperty("metadata_journal_title")
    private String journalTitle;
    @JsonProperty("metadata_article_title")
    private String articleTitle;
    @JsonProperty("metadata_issue")
    private String issue;
    @JsonProperty("metadata_editor")
    private String editor;
    @JsonProperty("metadata_chapter")
    private String chapter;
    @JsonProperty("metadata_chapter_title")
    private String chapterTitle;
    @JsonProperty("metadata_chapter_author")
    private String chapterAuthor;
    @JsonProperty("metadata_year")
    private String year;
    @JsonProperty("metadata_pages")
    private String pages;
    @JsonProperty("metadata_source")
    private String source;
    @JsonProperty("metadata_series_title_number")
    private String seriesTitleNumber;
    @JsonProperty("metadata_pmid")
    private String pmid;
    @JsonProperty("metadata_doi")
    private String doi;
    @JsonProperty("metadata_volume")
    private String volume;
    @JsonProperty("metadata_start_page")
    private String startPage;
    @JsonProperty("metadata_end_page")
    private String endPage;
    @JsonProperty("metadata_start_page2")
    private String startPage2;
    @JsonProperty("metadata_end_page2")
    private String endPage2;
    @JsonProperty("metadata_author_initials")
    private String authorInitials;
    @JsonProperty("metadata_part")
    private String part;
    @JsonProperty("additional_title")
    private String additionalTitle;
    @JsonProperty("additional_author")
    private String additionalAuthor;
    @JsonProperty("copyright_status")
    private String copyrightStatus;
    @JsonProperty("public_note")
    private String publicNote;
    @JsonProperty("license_type")
    private String licenseType;
    @JsonProperty("type")
    private String type;
    @JsonProperty("status_value")
    private String statusValue;
    @JsonProperty("status_desc")
    private String statusDescription;
    @JsonProperty("copyrights_status_value")
    private String copyrightStatusValue;
    @JsonProperty("copyrights_status_desc")
    private String copyrightStatusDescription;
    @JsonProperty("type_value")
    private String typeValue;
    @JsonProperty("type_desc")
    private String typeDescription;
    @JsonProperty("secondary_type_value")
    private String secondaryTypeValue;
    @JsonProperty("secondary_type_desc")
    private String secondaryTypeDescription;
    @JsonProperty("fk_reading_list_id")
    private String readingListId;
    @JsonProperty(DBH_INSTITUTION_CODE)
    private String dbhInstitutionCode;
    @JsonProperty(DBH_INSTITUTION_NAME)
    private String dbhInstitutionName;
    @JsonProperty(DBH_DEPT_CODE)
    private String dbhDeptCode;
    @JsonProperty(DBH_DEPT_NAME)
    private String dbhDeptName;
    @JsonProperty(DBH_DEPT_CODE_SSB)
    private String dbhDeptCodeSsb;
    @JsonProperty(DBH_YEAR)
    private String dbhYear;
    @JsonProperty(DBH_SEMESTER)
    private String dbhSemester;
    @JsonProperty(DBH_SEMESTER_NAME)
    private String dbhSemesterName;
    @JsonProperty(DBH_COURSE_STUDY_PROGRAM_CODE)
    private String dbhCourseStudyProgramCode;
    @JsonProperty(DBH_COURSE_STUDY_PROGRAM_NAME)
    private String dbhCourseStudyProgramName;
    @JsonProperty(DBH_COURSE_CODE)
    private String dbhCourseCode;
    @JsonProperty(DBH_COURSE_NAME)
    private String dbhCourseName;
    @JsonProperty(DBH_LEVEL_CODE)
    private String dbhLevelCode;
    @JsonProperty(DBH_LEVEL_NAME)
    private String dbhLevelName;
    @JsonProperty(DBH_COURSE_CREDIT)
    private String dbhCourseCredit;
    @JsonProperty(DBH_NUS_CODE)
    private String dbhNusCode;
    @JsonProperty(DBH_STATUS)
    private String dbhStatus;
    @JsonProperty(DBH_STATUS_NAME)
    private String dbhStatusName;
    @JsonProperty(DBH_LANGUAGE)
    private String dbhLanguage;
    @JsonProperty(DBH_NAME)
    private String dbhName;
    @JsonProperty(DBH_AREA_CODE)
    private String dbhAreaCode;
    @JsonProperty(DBH_AREA_NAME)
    private String dbhAreaName;
}
