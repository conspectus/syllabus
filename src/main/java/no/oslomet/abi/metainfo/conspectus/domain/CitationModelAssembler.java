package no.oslomet.abi.metainfo.conspectus.domain;

import no.oslomet.abi.metainfo.conspectus.controller.CitationController;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;


@Component
public class CitationModelAssembler
        extends RepresentationModelAssemblerSupport<Citation, CitationModel> {

    public CitationModelAssembler() {
        super(CitationController.class, CitationModel.class);
    }

    @Override
    public CitationModel toModel(Citation citation) {
        CitationModel citationModel = instantiateModel(citation);
        Map<String, String> parameters = new HashMap<>();
        parameters.put("citationId", citation.getCitationId());
        citationModel.add(linkTo(
                methodOn(CitationController.class)
                        .getFiltered(parameters, null))
                .withSelfRel().expand(parameters));
        citationModel.setCitationId(citation.getCitationId());
        citationModel.setOrganisationCode(citation.getOrganisationCode());
        citationModel.setSubUnitCode(citation.getSubUnitCode());
        citationModel.setSubSubUnitCode(citation.getSubSubUnitCode());
        citationModel.setOrganisationName(citation.getOrganisationName());
        citationModel.setSubUnitName(citation.getSubUnitName());
        citationModel.setSubSubUnitName(citation.getSubSubUnitName());
        citationModel.setCourseCodeName(citation.getCourseCodeName());
        citationModel.setCourseName(citation.getCourseName());
        citationModel.setInstituteCodeB(citation.getInstituteCodeB());
        citationModel.setSemesterCode1(citation.getSemesterCode1());
        citationModel.setSemesterCode2(citation.getSemesterCode2());
        citationModel.setSemesterCode3(citation.getSemesterCode3());
        citationModel.setTermCode1(citation.getTermCode1());
        citationModel.setTermCode2(citation.getTermCode2());
        citationModel.setTermCode3(citation.getTermCode3());
        citationModel.setTermName1(citation.getTermName1());
        citationModel.setTermName2(citation.getTermName2());
        citationModel.setTermName3(citation.getTermName3());
        citationModel.setCourseYear(citation.getCourseYear());
        citationModel.setTitle(citation.getTitle());
        citationModel.setAuthor(citation.getAuthor());
        citationModel.setPublisher(citation.getPublisher());
        citationModel.setPublicationDate(citation.getPublicationDate());
        citationModel.setEdition(citation.getEdition());
        citationModel.setIsbn(citation.getIsbn());
        citationModel.setIssn(citation.getIssn());
        citationModel.setMmsId(citation.getMmsId());
        citationModel.setAdditionalPersonName(citation.getAdditionalPersonName());
        citationModel.setPlaceOfPublication(citation.getPlaceOfPublication());
        citationModel.setCallNumber(citation.getCallNumber());
        citationModel.setNote(citation.getNote());
        citationModel.setJournalTitle(citation.getJournalTitle());
        citationModel.setArticleTitle(citation.getArticleTitle());
        citationModel.setIssue(citation.getIssue());
        citationModel.setEditor(citation.getEditor());
        citationModel.setChapter(citation.getChapter());
        citationModel.setChapterTitle(citation.getChapterTitle());
        citationModel.setChapterAuthor(citation.getChapterAuthor());
        citationModel.setYear(citation.getYear());
        citationModel.setPages(citation.getPages());
        citationModel.setSource(citation.getSource());
        citationModel.setSeriesTitleNumber(citation.getSeriesTitleNumber());
        citationModel.setPmid(citation.getPmid());
        citationModel.setDoi(citation.getDoi());
        citationModel.setVolume(citation.getVolume());
        citationModel.setStartPage(citation.getStartPage());
        citationModel.setEndPage(citation.getEndPage());
        citationModel.setStartPage2(citation.getStartPage2());
        citationModel.setEndPage2(citation.getEndPage2());
        citationModel.setAuthorInitials(citation.getAuthorInitials());
        citationModel.setPart(citation.getPart());
        citationModel.setAdditionalTitle(citation.getAdditionalTitle());
        citationModel.setAdditionalAuthor(citation.getAdditionalAuthor());
        citationModel.setCopyrightStatus(citation.getCopyrightStatus());
        citationModel.setPublicNote(citation.getPublicNote());
        citationModel.setLicenseType(citation.getLicenseType());
        citationModel.setType(citation.getType());
        citationModel.setStatusValue(citation.getStatusValue());
        citationModel.setStatusDescription(citation.getStatusDescription());
        citationModel.setCopyrightStatusValue(citation.getCopyrightStatusValue());
        citationModel.setCopyrightStatusDescription(citation.getCopyrightStatusDescription());
        citationModel.setTypeValue(citation.getTypeValue());
        citationModel.setTypeDescription(citation.getTypeDescription());
        citationModel.setSecondaryTypeValue(citation.getSecondaryTypeValue());
        citationModel.setSecondaryTypeDescription(citation.getSecondaryTypeDescription());
        citationModel.setReadingListId(citation.getReadingListId());
        citationModel.setCourseId(citation.getCourseId());
        citationModel.setDbhInstitutionCode(citation.getDbhInstitutionCode());
        citationModel.setDbhInstitutionName(citation.getDbhInstitutionName());
        citationModel.setDbhDeptCode(citation.getDbhDeptCode());
        citationModel.setDbhDeptName(citation.getDbhDeptName());
        citationModel.setDbhDeptCodeSsb(citation.getDbhDeptCodeSsb());
        citationModel.setDbhYear(citation.getDbhYear());
        citationModel.setDbhSemester(citation.getDbhSemester());
        citationModel.setDbhSemesterName(citation.getDbhSemesterName());
        citationModel.setDbhCourseStudyProgramCode(citation.getDbhCourseStudyProgramCode());
        citationModel.setDbhCourseStudyProgramName(citation.getDbhCourseStudyProgramName());
        citationModel.setDbhCourseCode(citation.getDbhCourseCode());
        citationModel.setDbhCourseName(citation.getDbhCourseName());
        citationModel.setDbhLevelCode(citation.getDbhLevelCode());
        citationModel.setDbhLevelName(citation.getDbhLevelName());
        citationModel.setDbhCourseCredit(citation.getDbhCourseCredit());
        citationModel.setDbhNusCode(citation.getDbhNusCode());
        citationModel.setDbhStatus(citation.getDbhStatus());
        citationModel.setDbhStatusName(citation.getDbhStatusName());
        citationModel.setDbhLanguage(citation.getDbhLanguage());
        citationModel.setDbhName(citation.getDbhName());
        citationModel.setDbhAreaCode(citation.getDbhAreaCode());
        citationModel.setDbhAreaName(citation.getDbhAreaName());
        return citationModel;
    }

    @Override
    public CollectionModel<CitationModel> toCollectionModel(Iterable<? extends Citation> citations) {
        CollectionModel<CitationModel> citationModels = super.toCollectionModel(citations);
        citationModels.add(linkTo(methodOn(CitationController.class)
                .searchCitationAllFields(null, Pageable.unpaged(), null)).withSelfRel());
        return citationModels;
    }

    private List<CitationModel> toCitationModel(List<Citation> citations) {
        if (citations.isEmpty())
            return Collections.emptyList();
        Map<String, String> parameters = new HashMap<>();
        parameters.put("citationId", null);
        return citations.stream()
                .map(citation -> CitationModel.builder()
                        .citationId(citation.getCitationId())
                        .organisationCode(citation.getOrganisationCode())
                        .subUnitCode(citation.getSubUnitCode())
                        .subSubUnitCode(citation.getSubSubUnitCode())
                        .organisationName(citation.getOrganisationName())
                        .subUnitName(citation.getSubUnitName())
                        .subSubUnitName(citation.getSubSubUnitName())
                        .instituteCodeB(citation.getInstituteCodeB())
                        .courseCodeName(citation.getCourseCodeName())
                        .courseName(citation.getCourseName())
                        .semesterCode1(citation.getSemesterCode1())
                        .semesterCode2(citation.getSemesterCode2())
                        .semesterCode3(citation.getSemesterCode3())
                        .termCode1(citation.getTermCode1())
                        .termCode2(citation.getTermCode2())
                        .termCode3(citation.getTermCode3())
                        .termName1(citation.getTermName1())
                        .termName2(citation.getTermName2())
                        .termName3(citation.getTermName3())
                        .courseYear(citation.getCourseYear())
                        .title(citation.getTitle())
                        .author(citation.getAuthor())
                        .publisher(citation.getPublisher())
                        .publicationDate(citation.getPublicationDate())
                        .edition(citation.getEdition())
                        .isbn(citation.getIsbn())
                        .issn(citation.getIssn())
                        .mmsId(citation.getMmsId())
                        .additionalPersonName(citation.getAdditionalPersonName())
                        .placeOfPublication(citation.getPlaceOfPublication())
                        .callNumber(citation.getCallNumber())
                        .note(citation.getNote())
                        .journalTitle(citation.getJournalTitle())
                        .articleTitle(citation.getArticleTitle())
                        .issue(citation.getIssue())
                        .editor(citation.getEditor())
                        .chapter(citation.getChapter())
                        .chapterTitle(citation.getChapterTitle())
                        .chapterAuthor(citation.getChapterAuthor())
                        .year(citation.getYear())
                        .pages(citation.getPages())
                        .source(citation.getSource())
                        .seriesTitleNumber(citation.getSeriesTitleNumber())
                        .pmid(citation.getPmid())
                        .doi(citation.getDoi())
                        .volume(citation.getVolume())
                        .startPage(citation.getStartPage())
                        .endPage(citation.getEndPage())
                        .startPage2(citation.getStartPage2())
                        .endPage2(citation.getEndPage2())
                        .authorInitials(citation.getAuthorInitials())
                        .part(citation.getPart())
                        .additionalTitle(citation.getAdditionalTitle())
                        .additionalAuthor(citation.getAdditionalAuthor())
                        .copyrightStatus(citation.getCopyrightStatus())
                        .publicNote(citation.getPublicNote())
                        .licenseType(citation.getLicenseType())
                        .type(citation.getType())
                        .statusValue(citation.getStatusValue())
                        .statusDescription(citation.getStatusDescription())
                        .copyrightStatusValue(citation.getCopyrightStatusValue())
                        .copyrightStatusDescription(citation.getCopyrightStatusDescription())
                        .typeValue(citation.getTypeValue())
                        .typeDescription(citation.getTypeDescription())
                        .secondaryTypeValue(citation.getSecondaryTypeValue())
                        .secondaryTypeDescription(citation.getSecondaryTypeDescription())
                        .readingListId(citation.getReadingListId())
                        .courseId(citation.getCourseId())
                        .dbhInstitutionCode(citation.getDbhInstitutionCode())
                        .dbhInstitutionName(citation.getDbhInstitutionName())
                        .dbhDeptCode(citation.getDbhDeptCode())
                        .dbhDeptName(citation.getDbhDeptName())
                        .dbhDeptCodeSsb(citation.getDbhDeptCodeSsb())
                        .dbhYear(citation.getDbhYear())
                        .dbhSemester(citation.getDbhSemester())
                        .dbhSemesterName(citation.getDbhSemesterName())
                        .dbhCourseStudyProgramCode(citation.getDbhCourseStudyProgramCode())
                        .dbhCourseStudyProgramName(citation.getDbhCourseStudyProgramName())
                        .dbhCourseCode(citation.getDbhCourseCode())
                        .dbhCourseName(citation.getDbhCourseName())
                        .dbhLevelCode(citation.getDbhLevelCode())
                        .dbhLevelName(citation.getDbhLevelName())
                        .dbhCourseCredit(citation.getDbhCourseCredit())
                        .dbhNusCode(citation.getDbhNusCode())
                        .dbhStatus(citation.getDbhStatus())
                        .dbhStatusName(citation.getDbhStatusName())
                        .dbhLanguage(citation.getDbhLanguage())
                        .dbhName(citation.getDbhName())
                        .dbhAreaCode(citation.getDbhAreaCode())
                        .dbhAreaName(citation.getDbhAreaName())
                        .build()
                        .add(linkTo(
                                methodOn(CitationController.class)
                                        .getFiltered(parameters, null))
                                .withSelfRel().expand(parameters)))
                .collect(Collectors.toList());
    }
}

