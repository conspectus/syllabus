package no.oslomet.abi.metainfo.conspectus.domain;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Repository;

@Repository
public interface CitationRepository extends ElasticsearchRepository<Citation,String> {
    Citation findByCitationId(String citationId);
    @NonNull Page<Citation> findAll(@NonNull Pageable pageable);
}
