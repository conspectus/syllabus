package no.oslomet.abi.metainfo.conspectus.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Detail {

    @JsonProperty
    String key;
    @JsonProperty
    String value;
    @JsonProperty
    String type;

    public Detail(String key, String value, String type) {
        this.key = key;
        this.value = value;
        this.type = type;
    }
}
