package no.oslomet.abi.metainfo.conspectus.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

public class Details {

    @JsonProperty
    List<Detail> details = new ArrayList<>();

    public Details() {
    }

    public void addDetail(Detail detail) {
        details.add(detail);
    }

    public int getSize() {
        return details.size();
    }
}
