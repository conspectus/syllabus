package no.oslomet.abi.metainfo.conspectus.security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authorization.AuthorizationDecision;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.util.matcher.IpAddressMatcher;

import static org.springframework.http.HttpMethod.GET;
import static org.springframework.http.HttpMethod.POST;
import static org.springframework.security.config.Customizer.withDefaults;

@EnableWebSecurity
@Configuration
public class SyllabusWebSecurityConfig {
    // @formatter:off
    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http)
            throws Exception {
        IpAddressMatcher hasIpAddress = new IpAddressMatcher("127.0.0.1");
        http
                .httpBasic(withDefaults())
                .csrf(AbstractHttpConfigurer::disable)
                .authorizeHttpRequests(authorize -> authorize
                        .requestMatchers(GET, "/details/**")
                        .permitAll()
                        .requestMatchers(GET, "/")
                        .permitAll()
                        .requestMatchers(GET, "/citations**")
                        .permitAll()
                        .requestMatchers(GET, "/search/**")
                        .permitAll()
                        .requestMatchers(POST, "/citation")
                        .authenticated()
                        .requestMatchers(POST, "/citation")
                        .access((authentication, context) ->
                                new AuthorizationDecision(
                                        hasIpAddress.matches(
                                                context.getRequest())))
                        .anyRequest().authenticated()
                );
        return http.build();
    }
    // @formatter:on
}
