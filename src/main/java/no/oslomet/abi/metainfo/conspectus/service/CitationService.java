package no.oslomet.abi.metainfo.conspectus.service;

import co.elastic.clients.elasticsearch._types.FieldValue;
import co.elastic.clients.elasticsearch._types.aggregations.*;
import co.elastic.clients.elasticsearch._types.query_dsl.BoolQuery;
import co.elastic.clients.elasticsearch._types.query_dsl.MultiMatchQuery;
import no.oslomet.abi.metainfo.conspectus.domain.Citation;
import no.oslomet.abi.metainfo.conspectus.domain.CitationRepository;
import no.oslomet.abi.metainfo.conspectus.domain.Detail;
import no.oslomet.abi.metainfo.conspectus.domain.Details;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.client.elc.ElasticsearchAggregations;
import org.springframework.data.elasticsearch.client.elc.NativeQuery;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.data.elasticsearch.core.SearchHitSupport;
import org.springframework.data.elasticsearch.core.SearchHits;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static co.elastic.clients.elasticsearch._types.query_dsl.Operator.And;
import static co.elastic.clients.elasticsearch._types.query_dsl.TextQueryType.CrossFields;
import static no.oslomet.abi.metainfo.conspectus.utils.Constants.*;
import static org.springframework.http.HttpStatus.*;

@Service
public class CitationService
        implements ICitationService {

    private final CitationRepository citationRepository;
    private final ElasticsearchOperations operations;

    public CitationService(CitationRepository citationRepository,
                           ElasticsearchOperations operations) {
        this.citationRepository = citationRepository;
        this.operations = operations;
    }

    @Override
    public Details getDetails(Map<String, String> queryParameters) {
        BoolQuery.Builder queryBuilder = getBoolQuery(queryParameters);
        // 200 is an arbitrary value. It is likely that no institute has that
        // many course codes, but at the moment I don't have a number
        String fieldType = getFieldCourse(queryParameters);
        if (queryParameters.isEmpty()) {
            queryParameters.put(ORGANISATION_CODE, "");
        }

        var query = buildQuery(fieldType, queryBuilder);
        Details details = getAggregationResults(fieldType, query);

        // If we are dealing with a subunit, we may need to jump over subsubunit
        if (details.getSize() == 0 && fieldType.equals(SUB_SUB_UNIT_CODE)) {
            fieldType = COURSE_CODE;
            queryBuilder = getBoolQuery(queryParameters);
            query = buildQuery(fieldType, queryBuilder);
            return getAggregationResults(fieldType, query);
        }
        return details;
    }

    @Override
    public Page<Citation> searchCitationsAnyField(
            String queryString, Pageable pageable,
            Map<String, String> queryParameters) {
        MultiMatchQuery.Builder mm = getMultiMatchQuery(queryString);
        BoolQuery.Builder queryBuilder = getBoolQuery(queryParameters);
        queryBuilder.must(mm.build()._toQuery());
        var query = NativeQuery.builder()
                .withQuery(queryBuilder.build()._toQuery())
                .withPageable(PageRequest.of(
                        pageable.getPageNumber(),
                        pageable.getPageSize()))
                .build();
        return getCitations(pageable, query);
    }

    @Override
    public Citation addCitation(Citation citation) {
        if (null == citationRepository
                .findByCitationId(citation.getCitationId())) {
            citationRepository.save(citation);
            return citation;
        }
        throw new ResponseStatusException(CONFLICT);
    }

    @Override
    public Page<Citation> searchCitationsUsingMap(
            Map<String, String> queryParameters, Pageable pageable) {
        var queryBuilder = new BoolQuery.Builder();
        for (Map.Entry<String, String> queryParameter :
                queryParameters.entrySet()) {
            // Can only search on known keys
            if (!PARAMETERS.containsKey(queryParameter.getKey())) {
                throw new ResponseStatusException(BAD_REQUEST,
                        queryParameter.getKey() + " is not a valid filter key");
            }
            queryBuilder.should(q -> q.match(m -> m
                    .field(queryParameter.getKey())
                    .query(queryParameter.getValue())));
        }
        var query = NativeQuery.builder()
                .withQuery(b -> b.bool(queryBuilder.build()))
                .withPageable(PageRequest.of(
                        pageable.getPageNumber(),
                        pageable.getPageSize()))
                .build();
        return getCitations(pageable, query);
    }

    private Details getAggregationResults(String fieldType, NativeQuery query) {
        SearchHits<Citation> searchHits = operations.search(query, Citation.class);
        if (!searchHits.hasAggregations()) {
            throw new ResponseStatusException(INTERNAL_SERVER_ERROR);
        }
        var aggregations =
                (ElasticsearchAggregations) searchHits.getAggregations();
        Buckets<CompositeBucket> buckets = aggregations
                .get("results")
                .aggregation()
                .getAggregate()
                .composite()
                .buckets();

        Details details = new Details();
        for (CompositeBucket bucket : buckets.array()) {
            Map<String, FieldValue> map = bucket.key();
            String code = map.get(CODE).stringValue();
            String name = code;
            if (map.containsKey(NAME)) {
                name = map.get(NAME).stringValue();
            }
            details.addDetail(new Detail(code, name, fieldType));
        }
        return details;
    }

    private NativeQuery buildQuery(String fieldType, BoolQuery.Builder queryBuilder) {

        final String fieldName = FIELD_PARAMETERS.get(fieldType);

        Aggregation compositeNameAggregation;

        if (!fieldName.isEmpty()) {
            compositeNameAggregation =
                    new Aggregation.Builder()
                            .composite(CompositeAggregation.of(ca -> ca
                                    .sources(Map.of(CODE,
                                                    CompositeAggregationSource.of(
                                                            caTerm -> caTerm.terms(
                                                                    CompositeTermsAggregation.of(
                                                                            caField -> caField.field(
                                                                                    fieldType + ".keyword"))
                                                            )
                                                    )
                                            )
                                    )
                                    // size is a guess. No value set gives max 10. Not sure what the upperbound should be
                                    // or how to calculate it at the moment. 300 is enough for the moment
                                    .size(300)
                                    .sources(Map.of(NAME,
                                                    CompositeAggregationSource.of(
                                                            caTerm -> caTerm.terms(
                                                                    CompositeTermsAggregation.of(
                                                                            caField -> caField.field(
                                                                                    fieldName + ".keyword")
                                                                    )
                                                            )
                                                    )
                                            )
                                    ).size(300)
                            ))
                            .build();
        } else {
            compositeNameAggregation =
                    new Aggregation.Builder()
                            .composite(CompositeAggregation.of(ca -> ca
                                    .sources(Map.of(CODE,
                                                    CompositeAggregationSource.of(
                                                            caTerm -> caTerm.terms(
                                                                    CompositeTermsAggregation.of(
                                                                            caField -> caField.field(
                                                                                    fieldType + ".keyword")
                                                                    )
                                                            )
                                                    )
                                            )
                                    )
                                    .size(300)))
                            .build();
        }
        return NativeQuery.builder()
                .withMaxResults(0)
                .withAggregation("results", compositeNameAggregation)
                .withQuery(queryBuilder.build()._toQuery())
                .build();
    }

    private MultiMatchQuery.Builder getMultiMatchQuery(String queryString) {
        return new MultiMatchQuery.Builder()
                .operator(And)
                .type(CrossFields)
                .query(queryString)
                .fields(ALL_ATTRIBUTES);
    }

    private BoolQuery.Builder getBoolQuery(Map<String, String> queryParameters) {
        var queryBuilder = new BoolQuery.Builder();
        for (Map.Entry<String, String> queryParameter :
                queryParameters.entrySet()) {
            // Can only search on known keys
            if (!ORG_COURSE_PARAMETERS.containsKey(queryParameter.getKey())) {
                throw new ResponseStatusException(BAD_REQUEST);
            }
            queryBuilder.must(q -> q.match(m -> m
                    .field(queryParameter.getKey())
                    .query(queryParameter.getValue())));
        }
        return queryBuilder;
    }


    @SuppressWarnings("unchecked")
    private Page<Citation> getCitations(Pageable pageable, NativeQuery query) {
        SearchHits<Citation> searchHits =
                operations.search(query, Citation.class);
        List<Citation> content = (List<Citation>)
                SearchHitSupport.unwrapSearchHits(searchHits);
        if (null == content) {
            return new PageImpl<>(new ArrayList<>(), pageable, 0);
        }
        return new PageImpl<>(content, query.getPageable(),
                searchHits.getTotalHits());
    }

    private String getFieldCourse(Map<String, String> queryParameters) {
        if (queryParameters.containsKey(COURSE_YEAR)) {
            return SEMESTER_CODE;
        } else if (queryParameters.containsKey(COURSE_CODE)) {
            return COURSE_YEAR;
        } else if (queryParameters.containsKey(SUB_SUB_UNIT_CODE)) {
            return COURSE_CODE;
        } else if (queryParameters.containsKey(SUB_UNIT_CODE)) {
            return SUB_SUB_UNIT_CODE;
        } else if (queryParameters.containsKey(ORGANISATION_CODE)) {
            return SUB_UNIT_CODE;
        }
        return ORGANISATION_CODE;
    }
}
