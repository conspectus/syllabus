package no.oslomet.abi.metainfo.conspectus.service;

import no.oslomet.abi.metainfo.conspectus.domain.Citation;
import no.oslomet.abi.metainfo.conspectus.domain.Details;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Map;

public interface ICitationService {

    Details getDetails(Map<String, String> queryParameters);

    Page<Citation> searchCitationsAnyField(
            String queryString, Pageable pageable,
            Map<String, String> queryParameters);

    Citation addCitation(Citation citation);

    Page<Citation> searchCitationsUsingMap(Map<String, String> queryParameters, Pageable pageable);
}
