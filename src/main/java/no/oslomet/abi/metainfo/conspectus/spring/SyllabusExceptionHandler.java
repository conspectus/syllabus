package no.oslomet.abi.metainfo.conspectus.spring;

import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import static org.springframework.http.HttpStatus.BAD_REQUEST;

@ControllerAdvice
public class SyllabusExceptionHandler
        extends ResponseEntityExceptionHandler {
    public SyllabusExceptionHandler() {
        super();
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<Object> handleAllException(
            final Exception ex, final WebRequest request) {
        return handleExceptionInternal(ex, BAD_REQUEST.toString(),
                new HttpHeaders(), BAD_REQUEST, request);
    }
}
