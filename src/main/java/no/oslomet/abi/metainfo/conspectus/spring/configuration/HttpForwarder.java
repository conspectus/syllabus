package no.oslomet.abi.metainfo.conspectus.spring.configuration;

import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.filter.ForwardedHeaderFilter;

@Configuration
public class HttpForwarder {
    @Bean
    public FilterRegistrationBean forwardedHeaderFilterRegistration() {
        ForwardedHeaderFilter filter = new ForwardedHeaderFilter();
        FilterRegistrationBean<ForwardedHeaderFilter> registration = new FilterRegistrationBean<>(filter);
        registration.setName("forwardedHeaderFilter");
        registration.setOrder(10000);
        return registration;
    }
}
