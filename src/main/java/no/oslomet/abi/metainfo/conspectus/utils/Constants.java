package no.oslomet.abi.metainfo.conspectus.utils;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static java.util.Map.entry;

public final class Constants {

    public static final String CODE = "code";
    public static final String NAME = "name";
    public static final String ORGANISATION_NAME = "cristin_unit_name_nb";
    public static final String ORGANISATION_CODE = "cristin_unit_code";
    public static final String SUB_UNIT_CODE = "cristin_sub_unit_code";
    public static final String SUB_SUB_UNIT_CODE = "cristin_sub_sub_unit_code";
    public static final String SEMESTER_CODE = "semester_code_1";
    public static final String SUB_UNIT_NAME = "cristin_sub_unit_name_nb";
    public static final String SUB_SUB_UNIT_NAME = "cristin_sub_sub_unit_name_nb";
    public static final String COURSE_YEAR = "course_year";
    public static final String COURSE_CODE = "conspectus_course_code";
    public static final String COURSE_NAME = "conspectus_course_name";
    //TODO: Does this value exist?
    public static final String COURSE_CODE_NAME = "conspectus_course_code_name";
    public static final String AUTHOR = "metadata_author";
    public static final String PUBLISHER = "metadata_publisher";
    public static final String PUBLICATION_DATE = "metadata_publication_date";
    public static final String ISSN = "metadata_issn";
    public static final String ISBN = "metadata_isbn";
    public static final String EDITION = "metadata_edition";
    public static final String JOURNAL_TITLE = "metadata_journal_title";
    public static final String ARTICLE_TITLE = "metadata_article_title";
    public static final String CHAPTER_TITLE = "metadata_chapter_title";
    public static final String PLACE_OF_PUBLICATION = "metadata_place_of_publication";
    public static final String ADDITIONAL_PERSON_NAME = "metadata_additional_person_name";
    public static final String CHAPTER_AUTHOR = "metadata_chapter_author";
    public static final String EDITOR = "metadata_editor";
    public static final String CHAPTER = "metadata_chapter";
    public static final String CALL_NUMBER = "metadata_call_number";
    public static final String PART = "metadata_part";
    public static final String SERIES_TITLE_NUMBER = "metadata_series_title_number";
    public static final String MMS_ID = "metadata_mms_id";
    public static final String ISSUE = "metadata_issue";
    public static final String START_PAGE_2 = "metadata_start_page_2";
    public static final String END_PAGE_2 = "metadata_end_page_2";
    public static final String YEAR = "metadata_year";
    public static final String SOURCE = "metadata_source";
    public static final String DOI = "metadata_doi";
    public static final String VOLUME = "metadata_volume";
    public static final String START_PAGE = "metadata_start_page";
    public static final String END_PAGE = "metadata_end_page";
    public static final String STATUS_VALUE = "metadata_status_value";
    public static final String STATUS_DESCRIPTION = "metadata_status_description";
    public static final String TYPE_VALUE = "metadata_type_value";
    public static final String TYPE_DESCRIPTION = "metadata_type_description";
    public static final String SECONDARY_TYPE_VALUE = "metadata_secondary_type_value";
    public static final String SECONDARY_TYPE_DESCRIPTION = "metadata_secondary_type_description";
    public static final String PAGES = "metadata_pages";

    public static final String DBH_INSTITUTION_CODE =
            "dbh_institution_code";
    public static final String DBH_INSTITUTION_NAME = "dbh_institution_name";
    public static final String DBH_DEPT_CODE = "dbh_dept_code";
    public static final String DBH_DEPT_NAME = "dbh_dept_name";
    public static final String DBH_DEPT_CODE_SSB =
            "dbh_dept_code_ssb";
    public static final String DBH_YEAR = "dbh_year";
    public static final String DBH_SEMESTER = "dbh_semester";
    public static final String DBH_SEMESTER_NAME = "dbh_semester_name";
    public static final String DBH_COURSE_STUDY_PROGRAM_CODE =
            "dbh_course_study_program_code";
    public static final String DBH_COURSE_STUDY_PROGRAM_NAME =
            "dbh_course_study_program_name";
    public static final String DBH_COURSE_CODE = "dbh_course_code";
    public static final String DBH_COURSE_NAME = "dbh_course_name";
    public static final String DBH_LEVEL_CODE = "dbh_level_code";
    public static final String DBH_LEVEL_NAME = "dbh_level_name";
    public static final String DBH_COURSE_CREDIT = "dbh_course_credit";
    public static final String DBH_NUS_CODE = "dbh_nus_code";
    public static final String DBH_STATUS = "dbh_status";
    public static final String DBH_STATUS_NAME = "dbh_status_name";
    public static final String DBH_LANGUAGE = "dbh_language";
    public static final String DBH_NAME = "dbh_name";
    public static final String DBH_AREA_CODE = "dbh_area_code";
    public static final String DBH_AREA_NAME = "dbh_area_name";

    // List of attributes it is possible to search on for building user
    // interface lists
    public static final Map<String, String> ORG_COURSE_PARAMETERS =
            Map.ofEntries(
                    entry(ORGANISATION_CODE, ""),
                    entry(SUB_UNIT_CODE, ""),
                    entry(SUB_SUB_UNIT_CODE, ""),
                    entry(SEMESTER_CODE, ""),
                    entry(COURSE_CODE, ""),
                    entry(COURSE_YEAR, "")
            );

    public static final Map<String, String> FIELD_PARAMETERS =
            Map.ofEntries(
                    entry(ORGANISATION_CODE, ORGANISATION_NAME),
                    entry(SUB_UNIT_CODE, SUB_UNIT_NAME),
                    entry(SUB_SUB_UNIT_CODE, SUB_SUB_UNIT_NAME),
                    entry(SEMESTER_CODE, "semester_1"),
                    entry(COURSE_CODE, COURSE_NAME),
                    entry(COURSE_YEAR, "course_year")
            );
    // List of attributes it is possible to search on
    public static final Map<String, String> PARAMETERS = Map.<String, String>ofEntries(
            entry("citationId", ""),
            entry(ORGANISATION_CODE, ""),
            entry(SUB_UNIT_CODE, ""),
            entry(SUB_SUB_UNIT_CODE, ""),
            entry(ORGANISATION_NAME, ""),
            entry(SUB_UNIT_NAME, ""),
            entry(SUB_SUB_UNIT_NAME, ""),
            entry(SEMESTER_CODE, ""),
            entry(COURSE_YEAR, ""),
            entry(AUTHOR, ""),
            entry(PUBLISHER, ""),
            entry(PUBLICATION_DATE, ""),
            entry(ISSN, ""),
            entry(ISBN, ""),
            entry(EDITION, ""),
            entry(JOURNAL_TITLE, ""),
            entry(ARTICLE_TITLE, ""),
            entry(CHAPTER_TITLE, ""),
            entry(PLACE_OF_PUBLICATION, ""),
            entry(ADDITIONAL_PERSON_NAME, ""),
            entry(CHAPTER_AUTHOR, ""),
            entry(EDITOR, ""),
            entry(CHAPTER, ""),
            entry(CALL_NUMBER, ""),
            entry(PART, ""),
            entry(SERIES_TITLE_NUMBER, ""),
            entry(MMS_ID, ""),
            entry(ISSUE, ""),
            entry(START_PAGE_2, ""),
            entry(END_PAGE_2, ""),
            entry(YEAR, ""),
            entry(SOURCE, ""),
            entry(DOI, ""),
            entry(VOLUME, ""),
            entry(START_PAGE, ""),
            entry(END_PAGE, ""),
            entry(STATUS_VALUE, ""),
            entry(STATUS_DESCRIPTION, ""),
            entry(TYPE_VALUE, ""),
            entry(TYPE_DESCRIPTION, ""),
            entry(SECONDARY_TYPE_VALUE, ""),
            entry(SECONDARY_TYPE_DESCRIPTION, ""),
            entry(PAGES, ""),
            entry(COURSE_CODE, ""),
            entry(DBH_INSTITUTION_CODE, ""),
            entry(DBH_INSTITUTION_NAME, ""),
            entry(DBH_DEPT_CODE, ""),
            entry(DBH_DEPT_NAME, ""),
            entry(DBH_DEPT_CODE_SSB, ""),
            entry(DBH_YEAR, ""),
            entry(DBH_SEMESTER, ""),
            entry(DBH_SEMESTER_NAME, ""),
            entry(DBH_COURSE_STUDY_PROGRAM_CODE, ""),
            entry(DBH_COURSE_STUDY_PROGRAM_NAME, ""),
            entry(DBH_COURSE_CODE, ""),
            entry(DBH_COURSE_NAME, ""),
            entry(DBH_LEVEL_CODE, ""),
            entry(DBH_LEVEL_NAME, ""),
            entry(DBH_COURSE_CREDIT, ""),
            entry(DBH_NUS_CODE, ""),
            entry(DBH_STATUS, ""),
            entry(DBH_STATUS_NAME, ""),
            entry(DBH_LANGUAGE, ""),
            entry(DBH_NAME, ""),
            entry(DBH_AREA_CODE, ""),
            entry(DBH_AREA_NAME, ""));

    public static final List<String> ALL_ATTRIBUTES =
            new ArrayList<>(PARAMETERS.keySet());

    private Constants() {
    }
}
